import fetch from 'isomorphic-unfetch';
import Layout from './Layout';

const Postdetails = ( props ) => {
	const { post } = props;

	return (
		<Layout title={ post.title.rendered }>
			Post Details Page ::: { post.title.rendered }
		</Layout>
	)
};

Postdetails.getInitialProps = async ( context ) => {

	const postId = context.query.id;

	const res = await fetch( `https://codeytek.com/wp-json/wp/v2/posts/${ postId }` );
	const postData = await res.json();

	return {
		post: postData
	}
};

export default Postdetails;