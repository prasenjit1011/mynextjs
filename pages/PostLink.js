import Link from 'next/link';
const PostLink = ( props ) => {
	return(
		<li>
			<Link as={`/postdetailspage/${ props.slug }-${ props.id }`} href={`/postdetails?id=${props.id}`}>
				<a>{ props.title }</a>
			</Link>
		</li>
	)
};

export default PostLink;
