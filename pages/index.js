import Link from 'next/link';
import Head from 'next/head';

const Index = () => {
	return <div>
		<Head>
			<title>Index</title>
		</Head>
		<h2>Index Page</h2>
		<Link href="/about">
			<a style={{"color":"#F00",}}>Go to about page</a>
		</Link>
	</div>
}

export default Index