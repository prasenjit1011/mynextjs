import Head from 'next/head';
import Link from 'next/link';
import Banner from './Banner';

const Layout = (props) => {
	return <div>
		<Head>
			<title>{props.title||'Next JS of React '}</title>
		</Head>
		<nav>
			<Link href="/"><a style={{"color":"#F07","padding":"10px"}}>Home</a></Link>
			<Link href="/about"><a style={{"color":"#F00","padding":"10px"}}>About Us</a></Link>
			<Link href="/contactus"><a style={{"color":"#F00","padding":"10px"}}>Contact Us</a></Link>
		</nav>
		<div>{props.children}</div><br />
		<Banner />
	</div>
}
 
export default Layout