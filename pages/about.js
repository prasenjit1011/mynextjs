import PostLink from "./PostLink";
import fetch from 'isomorphic-unfetch';
import Link from 'next/link';
import Layout from './Layout';

const About = (props) => {
	const { posts } = props;
	return <Layout title="Aboutus Page">
		<h2>About Page :::: Post Listing Page</h2>
		<Postlist id="hello-react" title="Hello React JS." />
		<Post id="hello-next-js" title="Hello Next JS" />

		{ posts.length && (
			<ul>
				{ posts.map( item => (
					<PostLink key={item.id} id={ item.id } slug={ item.slug } title = { item.title.rendered } />
				) ) }
			</ul>
		) }		
	</Layout>
}
About.getInitialProps = async () => {
	const res = await fetch( 'https://codeytek.com/wp-json/wp/v2/posts/' );
	const postsData = await res.json();
	return {
		posts: postsData
	}
};
const Postlist = (props) => {
	return <div>
		<Link href={`/post?title=${props.title}`}>
			<a>{props.title}</a>
		</Link>
	</div>
}
const Post = (props) => {
	return <div>
		<Link as={`/p/${props.id}`} href={`/post?title=${props.title}`}>
			<a>{props.title}</a>
		</Link>
	</div>
}

export default About